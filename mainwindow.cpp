#include "mainwindow.h"
#include "QtWebKit/QWebView"
#include <QUrl>
#include <QProcess>
#include <QWebFrame>
#include <QStringList>
#include <QLineEdit>
#include <QBoxLayout>
#include <QDockWidget>
#include <QWebElement>
#include <QWebPage>
#include <QWebElementCollection>
#include <QProcessEnvironment>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    commandProcess = new CommandThread(parent);
    scroll = 1000;
    lines = 0;
    row = 0;
    historySize = 30;
    webView = new QWebView(this);
    webView->load(QUrl("qrc:/html/default.html"));
    page = webView->page();
    frame = page->mainFrame();
    setCentralWidget(webView);

    //Makes the mainwindow available in javascript.
    frame->addToJavaScriptWindowObject("app",this);

    //Connect all the things
    connect(commandProcess, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
    connect(commandProcess, SIGNAL(readyReadStandardError()), this, SLOT(errorFromProcess()));
    connect(frame, SIGNAL(contentsSizeChanged(QSize)), this, SLOT(pageLoaded(QSize)));
    connect(commandProcess, SIGNAL(finished(int)), this, SLOT(close()));
    commandProcess->start("bash");
}
MainWindow::~MainWindow()
{
}
void  MainWindow::commandEntered(){
    QByteArray cmdLine = frame->documentElement().findFirst("#cmdLine").evaluateJavaScript("this.value").toString().append("\n").toAscii();
    commandProcess->write(cmdLine);
}
void MainWindow::dataAvailable(){
    content = frame->documentElement().findFirst("#content");
    content.appendInside("<div class='outputBlock'></div>");
    QWebElement outputBlock = content.lastChild();
    commandProcess->setReadChannel(QProcess::StandardOutput);
    while(commandProcess->canReadLine()){
        outputBlock.appendInside("<div class='outputItem row"+ QString::number(row % 2) + "'>" +  commandProcess->readLine() + "</div>");
        row = 1 - row;
        lines++;
        if(lines > historySize){
            content.findFirst(".outputItem").removeFromDocument();
            lines--;
        }
    }
    frame->evaluateJavaScript(QString("window.scrollTo(0,10000000000);"));
    webView->repaint();
    this->layout()->update();
}
void MainWindow::errorFromProcess(){
    content = frame->documentElement().findFirst("#content");
    content.appendInside("<div class='outputBlock errorBlock'></div>");
    QWebElement outputBlock = content.lastChild();
    commandProcess->setReadChannel(QProcess::StandardError);
    outputBlock.appendInside("<p>errorBlock</p>");
    while(commandProcess->canReadLine()){
        outputBlock.appendInside("<p>errorItem</p>");
        outputBlock.appendInside("<div class='outputItem row"+QString::number(row % 2) +"'>" + commandProcess->readLine() + "</div>");
        row = 1 - row;
        lines++;
        if(lines > historySize){
            content.findFirst(".outputItem").removeFromDocument();
            lines--;
        }
    }
    frame->evaluateJavaScript(QString("window.scrollTo(0,10000000000);"));
    webView->repaint();
    this->layout()->update();
    commandProcess->setReadChannel(QProcess::StandardOutput);
}
void MainWindow::pageLoaded(QSize size){
//    this->repaint();
}
void MainWindow::keyPressEvent(QKeyEvent* e){
    switch(e->key()){
    //Webview keys
    if(webView->hasFocus()){
        case Qt::Key_J:
            frame->setScrollPosition(frame->scrollPosition() + QPoint(0,15));
            break;
        case Qt::Key_K:
            frame->setScrollPosition(frame->scrollPosition() + QPoint(0,-15));
            break;
            if(e->modifiers() == Qt::ControlModifier){
                case Qt::Key_F:
                    frame->setScrollPosition(frame->scrollPosition() + QPoint(0,frame->geometry().height()));
                    break;
                case Qt::Key_B:
                    frame->setScrollPosition(frame->scrollPosition() + QPoint(0,-frame->geometry().height()));
                    break;
            }
    }
    // Global keys
    case Qt::Key_Escape:
            input->setFocus();
        break;
    case Qt::Key_F1:
        break;
    case Qt::Key_A:
        break;

    default: break;

    }
}
void MainWindow::resizeEvent(QResizeEvent * e){
    this->repaint();
}
