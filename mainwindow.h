#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWebKit/QWebView>
#include <QLineEdit>
#include <QProcess>
#include <QTextLayout>
#include <commandthread.h>
#include <QWebElement>


class MainWindow : public QMainWindow , public QTextLayout
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QWebView *webView;
    QWebPage *page;
    QWebFrame *frame;
    QLineEdit *input;
    QProcess *process;
    QString *output;
    QDockWidget *inputDock;
    CommandThread* commandProcess;
    QWebElement content;
    int scroll;
    int historySize;
    int row;
    int lines;

private:
protected:
    virtual void keyPressEvent(QKeyEvent *);
    virtual void resizeEvent(QResizeEvent *);


public slots:
    void commandEntered();
    void dataAvailable();
    void pageLoaded(QSize);
    void errorFromProcess();
};

#endif // MAINWINDOW_H
