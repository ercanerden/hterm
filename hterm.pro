#-------------------------------------------------
#
# Project created by QtCreator 2012-02-13T21:34:42
#
#-------------------------------------------------

QT       += core gui webkit

TARGET = hterm
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    commandthread.cpp

HEADERS  += mainwindow.h \
    commandthread.h

FORMS    +=
QMAKE_CXXFLAGS += -std=c++0x

RESOURCES += \
    application.qrc

OTHER_FILES += \
    default.html \
    style.css
